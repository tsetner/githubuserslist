# GitHub Users List #

## Application description ##

GitHub Users List is a simple app to download part of public GitHub users info. App shows list of usernames, allows to search by first letters and after username is clicked, it shows more detail info - icon and weblink. 

![detail_view.png](https://bitbucket.org/repo/nBRnxk/images/130622268-detail_view.png)

## Menu description ##

![menu.png](https://bitbucket.org/repo/nBRnxk/images/2837782277-menu.png)

1. Go to previous page
2. Go to next page
3. Request sync
4. Settings

## Details ##

App uses retrofit to fetch users info from GitHub API, syncadapter is responsible for that task.

Content provider is used to serve and insert data to sqlite database.

Last GitHub user id is >13.000.000, I have managed to fill my database with real 100.000 users, and it runs smoothly. But knowing database can grow to huge sizes I provided pagination (btw. it is probably big anti-pattern to store so much data on mobile device).

## Why ##

Application was created only for recruitment task as code test sample.