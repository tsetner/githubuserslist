package tsetner.org.bitbucket.githubusers.utils.pojos;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tomek on 24.07.15.
 */
public class User {

    @SerializedName("login")
    private String login;
    @SerializedName("id")
    private int id;
    @SerializedName("avatar_url")
    private String avatarUrl;
    @SerializedName("html_url")
    private String htmlPageUrl;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getHtmlPageUrl() {
        return htmlPageUrl;
    }

    public void setHtmlPageUrl(String htmlPageUrl) {
        this.htmlPageUrl = htmlPageUrl;
    }
}
