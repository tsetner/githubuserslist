package tsetner.org.bitbucket.githubusers.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tomek on 25.07.15.
 */
public class GitHubUsersDb extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "github_users.db";


    final String SQL_CREATE_USERS_TABLE = "CREATE TABLE " + GitHubUserDataContract.UserEntry.TABLE_NAME + " (" + GitHubUserDataContract.UserEntry._ID + " INTEGER PRIMARY KEY, " + GitHubUserDataContract.UserEntry.COLUMN_LOGIN + " TEXT, " +
            GitHubUserDataContract.UserEntry.COLUMN_AVATAR_URL + " TEXT, " + GitHubUserDataContract.UserEntry.COLUMN_HTML_PAGE_URL + " TEXT);";


    public GitHubUsersDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_USERS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + GitHubUserDataContract.UserEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

}
