package tsetner.org.bitbucket.githubusers.fragments;

import android.app.Dialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.ButterKnife;
import tsetner.org.bitbucket.githubusers.R;
import tsetner.org.bitbucket.githubusers.adapters.UserAdapter;
import tsetner.org.bitbucket.githubusers.data.GitHubUserDataContract;
import tsetner.org.bitbucket.githubusers.sync.GitHubSyncAdapter;


public class UsersListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private int mActivePage = 0;
    public static final int ITEMS_LOADING_PER_PAGE = 30;

    private UserAdapter mUserAdapter;

    @Bind(R.id.users_list)
    public ListView mUsersListView;

    @Bind(R.id.search_view)
    public EditText mSearchView;

    private boolean mIsRetained;

    public UsersListFragment() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        getLoaderManager().initLoader(0, null, this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpViews();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_users_list, container, false);
        ButterKnife.bind(this, v);
        setHasOptionsMenu(true);

        return v;
    }

    private void setUpViews() {
        mUserAdapter = new UserAdapter(getActivity(), null, 0);
        mUsersListView.setAdapter(mUserAdapter);

        setUpSearchView();
        setUpDetailsViewOnListItemClicked();
    }

    private void setUpDetailsViewOnListItemClicked() {
        mUsersListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, final long l) {
                new AsyncTask<Void, Void, String[]>() {

                    @Override
                    protected void onPostExecute(String[] strings) {
                        super.onPostExecute(strings);

                        Dialog dialog = new Dialog(getActivity());
                        dialog.setContentView(R.layout.dialog_user_details_layout);
                        dialog.setTitle(R.string.user_info);

                        TextView login = ButterKnife.findById(dialog, R.id.login);
                        TextView page = ButterKnife.findById(dialog, R.id.page);
                        ImageView icon = ButterKnife.findById(dialog, R.id.icon);

                        page.setText(strings[0]);
                        login.setText(strings[1]);
                        Picasso.with(getActivity()).load(strings[2]).into(icon);

                        dialog.getWindow().getAttributes().windowAnimations =
                                R.style.dialog_animation;
                        dialog.show();

                    }

                    @Override
                    protected String[] doInBackground(Void... voids) {


                        Cursor c = getActivity().getContentResolver().query(
                                GitHubUserDataContract.UserEntry.CONTENT_URI,
                                null,
                                GitHubUserDataContract.UserEntry._ID + "=?",
                                new String[]{Long.toString(l)},
                                null
                        );

                        c.moveToFirst();
                        String login = c.getString(c.getColumnIndex(GitHubUserDataContract.UserEntry.COLUMN_LOGIN));
                        String avatar = c.getString(c.getColumnIndex(GitHubUserDataContract.UserEntry.COLUMN_AVATAR_URL));
                        String page = c.getString(c.getColumnIndex(GitHubUserDataContract.UserEntry.COLUMN_HTML_PAGE_URL));

                        c.close();


                        return new String[]{page, login, avatar};
                    }
                }.execute();

            }

        });

    }

    private void setUpSearchView() {
        //re-query after search text changes
        //looking for users with whose nicks begin
        //with provided letters
        mSearchView.addTextChangedListener(new TextWatcher() {
            private Timer timer = new Timer();
            private final long DELAY = 100; // in ms

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                //if mIsRetained is true this method
                                //was called due to handling retained fragment
                                //so we shouldn't change page
                                if (!mIsRetained) {
                                    mActivePage = 0;
                                    getLoaderManager().restartLoader(0, null, UsersListFragment.this);
                                } else {
                                    mIsRetained = false;
                                }
                            }
                        });
                    }
                }, DELAY);
            }
        });

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        //ugly sqlite raw query parts to limit results,
        //sort by login without checking by capitals
        //and provide search query if there is one
        String orderBy = GitHubUserDataContract.UserEntry.COLUMN_LOGIN + " COLLATE NOCASE";
        orderBy += " LIMIT " + ITEMS_LOADING_PER_PAGE * mActivePage + "," + ITEMS_LOADING_PER_PAGE;

        String[] cols = new String[]{
                GitHubUserDataContract.UserEntry.COLUMN_LOGIN, GitHubUserDataContract.UserEntry._ID};

        if (mSearchView.getText().length() != 0) {
            String searchString = " like '" + mSearchView.getText().toString() + "%'";
            return new CursorLoader(getActivity(), GitHubUserDataContract.UserEntry.CONTENT_URI, cols, GitHubUserDataContract.UserEntry.COLUMN_LOGIN + searchString, null, orderBy);
        } else {
            return new CursorLoader(getActivity(), GitHubUserDataContract.UserEntry.CONTENT_URI, cols, null, null, orderBy);
        }


    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mUserAdapter.swapCursor(cursor);
        mUsersListView.smoothScrollToPositionFromTop(0, 0, 500);

    }


    public void requestSyncHandler() {
        GitHubSyncAdapter.syncImmediately(getActivity());

    }

    public void previousPageHandler() {
        if (mActivePage > 0) {
            mActivePage--;
            getLoaderManager().restartLoader(0, null, this);
        } else {
            Toast.makeText(getActivity(), R.string.first_page, Toast.LENGTH_SHORT).show();
        }
    }

    public void nextPageHandler() {
        if (mUserAdapter.getCount() == ITEMS_LOADING_PER_PAGE) {
            mActivePage++;
            getLoaderManager().restartLoader(0, null, this);
        } else {
            Toast.makeText(getActivity(), R.string.last_page, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mUserAdapter.swapCursor(null);
    }

    public void setRetainFlag() {
        mIsRetained = true;
    }
}
