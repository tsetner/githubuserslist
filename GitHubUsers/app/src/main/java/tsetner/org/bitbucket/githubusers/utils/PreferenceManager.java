package tsetner.org.bitbucket.githubusers.utils;

import android.content.Context;
import android.content.SharedPreferences;

import tsetner.org.bitbucket.githubusers.sync.GitHubSyncAdapter;
import tsetner.org.bitbucket.githubusers.utils.Consts;

/**
 * Created by tomek on 28.07.15.
 */
public class PreferenceManager {
    public static void setDoDownloadFullData(boolean flag, Context context) {
        android.preference.PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(
                Consts.DO_DOWNLOAD_FULL_KEY, flag
        ).apply();
    }

    public static boolean doDownloadFullData(Context context) {
        return android.preference.PreferenceManager.getDefaultSharedPreferences(context).getBoolean(Consts.DO_DOWNLOAD_FULL_KEY, false);
    }

    public static boolean doEnableAutoSync(Context context) {
        return android.preference.PreferenceManager.getDefaultSharedPreferences(context).getBoolean(Consts.DO_ENABLE_AUTO_SYNC, false);
    }

    public static boolean doEnableNotifications(Context context) {
        return android.preference.PreferenceManager.getDefaultSharedPreferences(context).getBoolean(Consts.DO_ENABLE_NOTIFICATIONS, false);
    }

    public static int getLastUserId(Context context) {
        SharedPreferences prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(Consts.LAST_USER_KEY, -1);
    }

    public static void setLastUserId(Context context, int id) {
        SharedPreferences prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putInt(Consts.LAST_USER_KEY, id).apply();
    }

    public static void setDoEnableAutoSync(boolean flag, Context context) {
        if (doEnableAutoSync(context) != flag) {
            android.preference.PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(
                    Consts.DO_ENABLE_AUTO_SYNC, flag
            ).apply();
            if (flag) {
                GitHubSyncAdapter.enableAutoSync(GitHubSyncAdapter.getSyncAccount(context),context);
            } else {
                GitHubSyncAdapter.disableAutoSync(GitHubSyncAdapter.getSyncAccount(context), context);
            }
        }
    }

    public static void setDoEnableNotifications(boolean flag, Context context) {
        android.preference.PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(
                Consts.DO_ENABLE_NOTIFICATIONS, flag
        ).apply();
    }
}
