package tsetner.org.bitbucket.githubusers.utils;

/**
 * Created by tomek on 28.07.15.
 */
public class Consts {
    public static final String DO_DOWNLOAD_FULL_KEY = "do_continue_download";
    public static final String DO_ENABLE_AUTO_SYNC= "do_enable_auto_sync";
    public static final String DO_ENABLE_NOTIFICATIONS = "do_enable_notifications";
    public static final String API_END_POINT = "https://api.github.com";
    public static final String LAST_USER_KEY = "last_user_id";

    //max is 100 for non-authenticated users
    public static final int USERS_PER_PART = 100;
}
