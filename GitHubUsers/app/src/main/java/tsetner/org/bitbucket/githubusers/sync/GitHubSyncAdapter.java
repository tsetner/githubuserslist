package tsetner.org.bitbucket.githubusers.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import tsetner.org.bitbucket.githubusers.R;
import tsetner.org.bitbucket.githubusers.activities.MainActivity;
import tsetner.org.bitbucket.githubusers.data.GitHubUserDataContract;
import tsetner.org.bitbucket.githubusers.data.GitHubUsersService;
import tsetner.org.bitbucket.githubusers.utils.Consts;
import tsetner.org.bitbucket.githubusers.utils.PreferenceManager;
import tsetner.org.bitbucket.githubusers.utils.pojos.User;

/**
 * Created by tomek on 10.08.15.
 */
public class GitHubSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final int USER_NOTIFICATION_ID = 1235;
    //75 min for default because api limits reset after 1 hour
    private static final int SYNC_INTERVAL = 60 * 75;

    //// TODO: change for flextime with syncrequest.builder

    public GitHubSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Consts.API_END_POINT)
                .build();

        List<User> users;

        GitHubUsersService gitHubUsersService = restAdapter.create(GitHubUsersService.class);

        if (PreferenceManager.doEnableNotifications(getContext())) {
            createAndShowNotificationWithText(getContext().getString(R.string.sync_in_progress));
        }
        do {
            int lastUserId = PreferenceManager.getLastUserId(getContext());
            try {
                users = gitHubUsersService.getUsersList(
                        lastUserId + 1,
                        Consts.USERS_PER_PART);
            } catch (RetrofitError e) {

                if (e.getKind() == RetrofitError.Kind.HTTP && e.getResponse().getStatus() == 403) {
                    if (PreferenceManager.doEnableNotifications(getContext())) {
                        createAndShowNotificationWithText(getContext().getString(R.string.api_limit_reached));
                    }
                } else if (e.getKind() == RetrofitError.Kind.NETWORK) {
                    if (PreferenceManager.doEnableNotifications(getContext())) {
                        createAndShowNotificationWithText(getContext().getString(R.string.connection_error));
                    }
                }
                return;
            }
            if (users.isEmpty()) {
                return;
            } else {
                insertData(users);
            }
        }
        while (doDownloadMore());
        notifySyncCompleted();
    }


    private void createAndShowNotificationWithText(String notificationTitle) {
        NotificationCompat.Builder mBuilder;
        Intent resultIntent;
        TaskStackBuilder stackBuilder;
        PendingIntent resultPendingIntent;
        NotificationManager mNotificationManager;
        mBuilder =
                new NotificationCompat.Builder(getContext())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getContext().getString(R.string.app_name)
                        )
                        .setContentText(notificationTitle)
        ;

        // Make something interesting happen when the user clicks on the notification.
        // In this case, opening the app is sufficient.
        resultIntent = new Intent(getContext(), MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        stackBuilder = TaskStackBuilder.create(getContext());
        stackBuilder.addNextIntent(resultIntent);
        resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager =
                (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        // USER_NOTIFICATION_ID allows you to update the notification later on.
        mNotificationManager.notify(USER_NOTIFICATION_ID, mBuilder.build());
    }



    private void notifySyncCompleted() {
        Context context = getContext();

        SharedPreferences prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        String displayNotificationsKey = context.getString(R.string.pref_enable_notifications_key);
        boolean displayNotifications = prefs.getBoolean(displayNotificationsKey,
                Boolean.parseBoolean(context.getString(R.string.pref_enable_notifications_default)));

        if (displayNotifications) {

            String lastNotificationKey = context.getString(R.string.pref_last_notification);
            long lastSync = prefs.getLong(lastNotificationKey, 0);

            Cursor cursor = context.getContentResolver().query(GitHubUserDataContract.UserEntry.COUNT_USERS_URI, null, null, null, null);

            if (cursor.moveToFirst()) {
                long usersCount = cursor.getLong(cursor.getColumnIndex(getContext().getString(R.string.COLUMN_COUNT_USERS)));

                if (PreferenceManager.doEnableNotifications(getContext())) {
                    String contentText = "Users count: " + Long.toString(usersCount);
                    if (lastSync != 0)
                        contentText += "\nlast sync: " + DateFormat.getDateTimeInstance().format(new Date(lastSync));
                    createAndShowNotificationWithText(contentText);
                }

                //refreshing last sync
                SharedPreferences.Editor editor = prefs.edit();
                editor.putLong(lastNotificationKey, System.currentTimeMillis());
                editor.commit();
            }
            cursor.close();

        }
    }


    private void insertData(List<User> users) {
        ContentValues values[] = new ContentValues[users.size()];

        for (int i = 0; i < users.size(); i++) {
            values[i] = new ContentValues();
            values[i].put(GitHubUserDataContract.UserEntry.COLUMN_LOGIN, users.get(i).getLogin());
            values[i].put(GitHubUserDataContract.UserEntry.COLUMN_AVATAR_URL, users.get(i).getAvatarUrl());
            values[i].put(GitHubUserDataContract.UserEntry.COLUMN_HTML_PAGE_URL, users.get(i).getHtmlPageUrl());

        }

        PreferenceManager.setLastUserId(getContext(), users.get(users.size() - 1).getId());

        getContext().getContentResolver().bulkInsert(
                GitHubUserDataContract.UserEntry.CONTENT_URI,
                values
        );
    }


    private boolean doDownloadMore() {
        return PreferenceManager.doDownloadFullData(getContext());
    }

    public static void initializeSyncAdapter(Context context) {
        getSyncAccount(context);
    }

    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(
                context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

        // If the password doesn't exist, the account doesn't exist
        String pass = accountManager.getPassword(newAccount);
        if (null == pass) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                return null;
            }

            onAccountCreated(newAccount, context);
        }
        return newAccount;
    }

    public static void configurePeriodicSync(Context context, int syncInterval) {
        Account account = getSyncAccount(context);
        String authority = context.getString(R.string.content_authority);

        ContentResolver.addPeriodicSync(account,
                authority, new Bundle(), syncInterval);
    }

    public static void enableAutoSync(Account newAccount, Context context) {
        /*
         * Since we've created an account
         */
        GitHubSyncAdapter.configurePeriodicSync(context, SYNC_INTERVAL);

        /*
         * Without calling setSyncAutomatically, our periodic sync will not be enabled.
         */
        ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), PreferenceManager.doEnableAutoSync(context));
    }

    public static void disableAutoSync(Account account, Context context) {
        ContentResolver.setSyncAutomatically(account, context.getString(R.string.content_authority), false);
    }


    private static void onAccountCreated(Account newAccount, Context context) {

 /*
         * Finally, let's do a sync to get things started
         */
        enableAutoSync(newAccount, context);
        syncImmediately(context);

    }

    public static void syncImmediately(Context context) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        ContentResolver.requestSync(getSyncAccount(context),
                context.getString(R.string.content_authority), bundle);
    }
}
