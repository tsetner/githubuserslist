package tsetner.org.bitbucket.githubusers.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Spinner;

import butterknife.ButterKnife;
import tsetner.org.bitbucket.githubusers.R;
import tsetner.org.bitbucket.githubusers.fragments.UsersListFragment;
import tsetner.org.bitbucket.githubusers.sync.GitHubSyncAdapter;
import tsetner.org.bitbucket.githubusers.utils.PreferenceManager;

public class MainActivity extends AppCompatActivity {

    private UsersListFragment mUsersListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUpBaseContent();
        ButterKnife.bind(this);
        GitHubSyncAdapter.initializeSyncAdapter(this);
    }

    private void setUpBaseContent() {
        setContentView(R.layout.activity_main);
        mUsersListFragment = (UsersListFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.users_fragment_tag));

        if (mUsersListFragment == null) {
            mUsersListFragment = new UsersListFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, mUsersListFragment, getString(R.string.users_fragment_tag)).commit();
        } else {
            mUsersListFragment.setRetainFlag();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, mUsersListFragment, getString(R.string.users_fragment_tag)).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        switch (id) {
            case R.id.action_settings: {
                buildAndShowSettingsDialog();
                return true;
            }
            case R.id.action_next: {
                mUsersListFragment.nextPageHandler();
                return true;
            }

            case R.id.action_previous: {
                mUsersListFragment.previousPageHandler();
                return true;
            }

            case R.id.action_download: {
                mUsersListFragment.requestSyncHandler();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }


    private void buildAndShowSettingsDialog() {
        //you can choose to download data with parts or
        //download as much as API allows
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this).
                setTitle(R.string.settings);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_settings, null);
        dialogBuilder.setView(dialogView);


        final CheckBox doEnableAutoSync = ButterKnife.findById(dialogView, R.id.do_sync_cb);
        doEnableAutoSync.setChecked(PreferenceManager.doEnableAutoSync(this));

        final CheckBox doEnableNotifications = ButterKnife.findById(dialogView,
                R.id.show_notifications);
        doEnableNotifications.setChecked(PreferenceManager.doEnableNotifications(this));

        final Spinner downloadMode = ButterKnife.findById(dialogView,
                R.id.download_mode);

        if (PreferenceManager.doDownloadFullData(this)) {
            downloadMode.setSelection(0);
        } else {
            downloadMode.setSelection(1);
        }

        dialogBuilder.
                setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (downloadMode.getSelectedItemPosition() == 0) {
                            PreferenceManager.setDoDownloadFullData(true, MainActivity.this);
                        } else {
                            PreferenceManager.setDoDownloadFullData(false, MainActivity.this);
                        }

                        PreferenceManager.setDoEnableAutoSync(doEnableAutoSync.isChecked(),
                                MainActivity.this);
                        PreferenceManager.setDoEnableNotifications(doEnableNotifications.isChecked(), MainActivity.this);

                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();

        alertDialog.show();
    }
}
