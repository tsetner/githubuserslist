package tsetner.org.bitbucket.githubusers.data;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;
import tsetner.org.bitbucket.githubusers.utils.pojos.User;

/**
 * Created by tomek on 25.07.15.
 */
public interface GitHubUsersService {
    @GET("/users")
    List<User> getUsersList(@Query("since") int sinceUserId, @Query("per_page") int usersPerPage);
}