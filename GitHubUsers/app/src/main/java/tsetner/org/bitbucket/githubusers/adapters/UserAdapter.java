package tsetner.org.bitbucket.githubusers.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import tsetner.org.bitbucket.githubusers.R;
import tsetner.org.bitbucket.githubusers.data.GitHubUserDataContract;

public class UserAdapter extends CursorAdapter {

    public UserAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_user, parent, false);
        ViewHolder holder = new ViewHolder(view);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        String userName = cursor.getString(cursor.getColumnIndex(GitHubUserDataContract.UserEntry.COLUMN_LOGIN));
        viewHolder.mUserName.setText(userName);
    }

    static class ViewHolder {
        @Bind(R.id.user_name_tv) TextView mUserName;
        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
