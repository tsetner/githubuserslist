package tsetner.org.bitbucket.githubusers.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;

import tsetner.org.bitbucket.githubusers.R;

/**
 * Created by tomek on 25.07.15.
 */
public class GitHubUserDataProvider extends ContentProvider {

    public static final int USERS = 100;
    public static final int COUNT_USERS = 200;

    private GitHubUsersDb mGitHubUsersDb;
    public static final UriMatcher sUriMatcher = buildUriMatcher();

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(GitHubUserDataContract.CONTENT_AUTHORITY, GitHubUserDataContract.UserEntry.USER_PATH, USERS);
        matcher.addURI(GitHubUserDataContract.CONTENT_AUTHORITY, GitHubUserDataContract.UserEntry.USER_COUNT_PATH, COUNT_USERS);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mGitHubUsersDb = new GitHubUsersDb(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        Cursor result;

        switch (sUriMatcher.match(uri)) {
            case USERS:
                result = getUsers(projection, selection, selectionArgs, sortOrder);
                break;
            case COUNT_USERS:
                result = getUsersCount(selection, selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        result.setNotificationUri(getContext().getContentResolver(), uri);
        return result;
    }

    private Cursor getUsersCount(String selection, String[] selectionArgs) {
        long counter = DatabaseUtils.queryNumEntries(mGitHubUsersDb.getReadableDatabase(), GitHubUserDataContract.UserEntry.TABLE_NAME, selection, selectionArgs);
        MatrixCursor matrixCursor = new MatrixCursor(new String[]{getContext().getString(R.string.COLUMN_COUNT_USERS)}, 1);
        matrixCursor.addRow(new Object[]{counter});
        return matrixCursor;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {

        final SQLiteDatabase db = mGitHubUsersDb.getWritableDatabase();
        switch (sUriMatcher.match(uri)) {
            case USERS: {
                return bulkInsertUsers(db, values, uri);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    private int bulkInsertUsers(SQLiteDatabase db, ContentValues[] values, Uri uri) {

        db.beginTransaction();
        String SQL_BULK = "INSERT INTO " + GitHubUserDataContract.UserEntry.TABLE_NAME + " (" +
                GitHubUserDataContract.UserEntry.COLUMN_LOGIN + ", " +
                GitHubUserDataContract.UserEntry.COLUMN_HTML_PAGE_URL + ", " +
                GitHubUserDataContract.UserEntry.COLUMN_AVATAR_URL + ") VALUES (?, ?, ?)";
        SQLiteStatement statement = db.compileStatement(SQL_BULK);


        for (ContentValues value : values) {
            statement.bindString(1, value.getAsString(GitHubUserDataContract.UserEntry.COLUMN_LOGIN));
            statement.bindString(2, value.getAsString(GitHubUserDataContract.UserEntry.COLUMN_HTML_PAGE_URL));
            statement.bindString(3, value.getAsString(GitHubUserDataContract.UserEntry.COLUMN_AVATAR_URL));
            statement.execute();
            statement.clearBindings();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        getContext().getContentResolver().notifyChange(uri, null);

        return values.length;
    }

    @Override
    public String getType(Uri uri) {
        throw new UnsupportedOperationException();
    }


    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        throw new UnsupportedOperationException();
    }

    private Cursor getUsers(String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        return mGitHubUsersDb.getReadableDatabase().query(
                GitHubUserDataContract.UserEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder
        );
    }
}
