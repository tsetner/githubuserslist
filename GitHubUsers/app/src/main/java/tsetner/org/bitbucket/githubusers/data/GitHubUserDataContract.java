package tsetner.org.bitbucket.githubusers.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by tomek on 25.07.15.
 */
public class GitHubUserDataContract {
    public static final String CONTENT_AUTHORITY = "tsetner.org.bitbucket.githubusers.data";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "users";

        //used by content provider
        public static final String USER_PATH = "user";
        public static final String USER_COUNT_PATH = "count_users";

        public static final String COLUMN_LOGIN = "login";
        public static final String COLUMN_AVATAR_URL = "avatar_url";
        public static final String COLUMN_HTML_PAGE_URL = "html_url";

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(USER_PATH).build();
        public static final Uri COUNT_USERS_URI = BASE_CONTENT_URI.buildUpon().appendPath(USER_COUNT_PATH).build();

    }

}
